/*
 * PopupPage.java
 * com.qasir.pages
 *
 * Created by Agus Marwanto
 * on Nov 17, 2021
 */

package com.qasir.pages;

import com.qasir.base.BasePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class PopupPage extends BasePage {

	@AndroidFindBy(id = "uikit_base_confirmation_dialog_positive_button")
	private MobileElement positiveBtn;

	@AndroidFindBy(id = "uikit_base_confirmation_dialog_negative_button")
	private MobileElement negativeBtn;

	@AndroidFindBy(id = "com.android.packageinstaller:id/permission_allow_button")
	private MobileElement allowBtn;

	public OnboardingPage tapNextBtn() {
		click(positiveBtn);
		return new OnboardingPage();
	}

	public void tapLaterBtn() {
		click(negativeBtn);
	}

	public void tapOKBtn() {
		click(positiveBtn);
	}

	public void tapAllowBtn() {
		click(allowBtn);
	}

	public void tapAllAllowBtn() {
		for (int i = 0; i < 2; i++) {
			tapAllowBtn();
		}
	}

}
