/*
 * ChooseLanguagePage.java
 * com.qasir.pages
 *
 * Created by Agus Marwanto
 * on Nov 17, 2021
 */

package com.qasir.pages;

import com.qasir.base.BasePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class LanguagePage extends BasePage{

	@AndroidFindBy(uiAutomator = "new UiSelector().resourceId(\"com.innovecto.etalastic:id/radio_button_localization\").text(\"Indonesia\")")
	private MobileElement indonesiaBtn;
	
	public PopupPage tapIndonesiaBtn() {
		click(indonesiaBtn);
		return new PopupPage();
	}

}
