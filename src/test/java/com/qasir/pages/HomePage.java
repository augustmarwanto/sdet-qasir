/*
 * HomePage.java
 * com.qasir.pages
 *
 * Created by Agus Marwanto
 * on Nov 18, 2021
 */

package com.qasir.pages;

import com.qasir.base.BasePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class HomePage extends BasePage {

	@AndroidFindBy(id = "ui_kit_toolbar_drawer_title")
	private MobileElement homeTitle;

	@AndroidFindBy(id = "button_start_selling")
	private MobileElement transactionBtn;

	@AndroidFindBy(xpath = "//android.widget.GridView/android.view.ViewGroup[3]")
	private MobileElement outletBtn;

	public String getHomeTitle() {
		return getText(homeTitle);
	}

	public boolean displayTransactionBtn() {
		return isDisplayed(transactionBtn);
	}

	public OutletPage name() {
		click(outletBtn);
		return new OutletPage();
	}

	public OutletPage tapOutletBtn() {
		click(outletBtn);
		return new OutletPage();
	}

}
