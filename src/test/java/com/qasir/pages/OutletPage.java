/*
 * OutletPage.java
 * com.qasir.pages
 *
 * Created by Agus Marwanto
 * on Nov 18, 2021
 */

package com.qasir.pages;

import com.qasir.base.BasePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class OutletPage extends BasePage {

	@AndroidFindBy(uiAutomator = "new UiSelector().resourceId(\"com.innovecto.etalastic:id/toolbar\").childSelector(new UiSelector().className(\"android.widget.TextView\"))")
	private MobileElement outletTitle;

	@AndroidFindBy(xpath = "//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]")
	private MobileElement outletList;

	public String getOutletTitle() {
		return getText(outletTitle);
	}

	public void tapOutletList() {
		click(outletList);
	}

}
