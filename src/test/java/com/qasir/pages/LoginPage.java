/*
 * LoginPage.java
 * com.qasir.pages
 *
 * Created by Agus Marwanto
 * on Nov 18, 2021
 */

package com.qasir.pages;

import com.qasir.base.BasePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class LoginPage extends BasePage {

	@AndroidFindBy(id = "com.innovecto.etalastic:id/login_content_customEditText_phone")
	private MobileElement phoneField;

	@AndroidFindBy(id = "com.innovecto.etalastic:id/login_content_customEditText_pin")
	private MobileElement pinField;

	public boolean displayPhoneField() {
		return isDisplayed(phoneField);
	}

	public boolean displayPinField() {
		return isDisplayed(pinField);
	}

	public LoginPage inputPhone(String phone) {
		sendKeys(phoneField, phone);
		return this;
	}

	public HomePage inputPin(String pin) {
		sendKeys(pinField, pin);
		return new HomePage();
	}

}
