/*
 * OnboardingPage.java
 * com.qasir.pages
 *
 * Created by Agus Marwanto
 * on Nov 17, 2021
 */

package com.qasir.pages;

import com.qasir.base.BasePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class OnboardingPage extends BasePage {
	
	@AndroidFindBy(id = "button_register")
	private MobileElement registerBtn;
	
	@AndroidFindBy(id = "button_login")
	private MobileElement loginBtn;
	
	public boolean displayRegisterBtn() {
		return isDisplayed(registerBtn);
	}
	
	public boolean displayLoginBtn() {
		return isDisplayed(loginBtn);
	}
	
	public LoginPage tapLoginBtn() {
		click(loginBtn);
		return new LoginPage();
	}

}
