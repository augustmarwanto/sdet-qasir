package com.qasir.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.qasir.base.BaseTest;
import com.qasir.pages.HomePage;
import com.qasir.pages.LanguagePage;
import com.qasir.pages.LoginPage;
import com.qasir.pages.OnboardingPage;
import com.qasir.pages.OutletPage;
import com.qasir.pages.PopupPage;

public class TechTest extends BaseTest {
	LanguagePage languagePage;
	OnboardingPage onboardingPage;
	PopupPage popupPage;
	LoginPage loginPage;
	HomePage homePage;
	OutletPage outletPage;

	@BeforeMethod
	public void beforeMethod() {
		languagePage = new LanguagePage();
		popupPage = new PopupPage();
		outletPage = new OutletPage();
	}

	@Test
	public void shouldDisplayHomePage() {
		onboardingPage = languagePage.tapIndonesiaBtn()
				.tapNextBtn();
		Assert.assertTrue(onboardingPage.displayRegisterBtn());
		Assert.assertTrue(onboardingPage.displayLoginBtn());

		loginPage = onboardingPage.tapLoginBtn();
		popupPage.tapOKBtn();
		popupPage.tapAllAllowBtn();
		Assert.assertTrue(loginPage.displayPhoneField());
		Assert.assertTrue(loginPage.displayPinField());

		homePage = loginPage.inputPhone(auth.getProperty("phoneNumber"))
				.inputPin(auth.getProperty("pin"));
		outletPage.tapOutletList();
		popupPage.tapAllowBtn();
		popupPage.tapLaterBtn();
		Assert.assertEquals(homePage.getHomeTitle(), "Beranda");
		Assert.assertTrue(homePage.displayTransactionBtn());
	}

}
