/*
 * BasePage.java
 * com.qasir.base
 *
 * Created by Agus Marwanto
 * on Nov 17, 2021
 */

package com.qasir.base;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class BasePage extends BaseTest {

	public BasePage() {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	public void waitForVisible(MobileElement el) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(el));
	}
	
	public void waitForClickable(MobileElement el) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(el));
	}

	public void click(MobileElement el) {
		waitForClickable(el);
		el.click();
	}
	
	public boolean isEnabled(MobileElement el) {
		return el.isEnabled();
	}

	public boolean isDisplayed(MobileElement el) {
		waitForVisible(el);
		return el.isDisplayed();
	}

	public void sendKeys(MobileElement el, String txt) {
		waitForVisible(el);
		el.sendKeys(txt);
	}
	
	public String getText(MobileElement el) {
		waitForVisible(el);
		return el.getText();
	}

}
