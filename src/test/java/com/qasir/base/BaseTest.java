/*
 * BaseTest.java
 * com.qasir.base
 *
 * Created by Agus Marwanto
 * on Nov 17, 2021
 */

package com.qasir.base;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class BaseTest {
	protected static AndroidDriver<MobileElement> driver;
	private Properties config;
	public Properties auth;

	@Parameters({ "platformName", "platformVersion", "deviceName", "udid" })
	@BeforeTest
	public void beforeTest(String platformName, String platformVersion, String deviceName,
			String udid) throws IOException {
		
		config = load("config.properties");
		auth = load("auth.properties");

		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("platformName", platformName);
		caps.setCapability("platformVersion", platformVersion);
		caps.setCapability("deviceName", deviceName);
		caps.setCapability("udid", udid);
		caps.setCapability("appPackage", config.getProperty("appPackage"));
		caps.setCapability("appActivity", config.getProperty("appActivity"));

		URL remoteUrl = new URL("http://127.0.0.1:4723/wd/hub");
		driver = new AndroidDriver<>(remoteUrl, caps);
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}

	public Properties load(String fileName) throws IOException {
		Properties props = new Properties();
		InputStream configStream = getClass().getClassLoader().getResourceAsStream(fileName);
		props.load(configStream);
		return props;
	}

}
