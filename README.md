# Mobile Appium Tests

Mobile Automation Test for Technical Test at Qasir.

## How do I run tests?

Use new terminal to run the test.

```bash
git clone https://gitlab.com/augustmarwanto/sdet-qasir.git
```

```bash
mvn test
```

Make sure the Appium is running at local mechine before run test.

## Required

```java
Please make sure to install and setting the following.

- Install Appium on a local machine
- Install Maven on a local machine
- Install Java
- Setting up Android for Developer Mode
- Install Qasir apk on Android Mobile
- Change phone number and pin at src/test/resources/auth.properties
- Change your platform detail at android.xml
```
